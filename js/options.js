function save_options() {
  var gitlabUri = document.getElementById('gitlaburi').value;
  var headroom = document.getElementById('headroom').checked;

  chrome.permissions.request({
          origins: [gitlabUri + "/*"]
        }, function(granted) {
          // The callback argument will be true if the user granted the permissions.
          if (granted) {
            chrome.storage.sync.set({
              gitlabUri: gitlabUri,
              headroom: headroom
            }, function() {
              // Update status to let user know options were saved.
              var status = document.getElementById('status');
              status.textContent = 'Settings saved.';
              setTimeout(function() {
                status.textContent = '';
              }, 1000);
            });
          } else {
            // TODO: Add something here

          }
        });
}

function restore_options() {
  chrome.storage.sync.get({
    gitlabUri: 'https://gitlab.com',
    headroom: true
  }, function(items) {
    document.getElementById('gitlaburi').value = items.gitlabUri;
    document.getElementById('headroom').checked = items.headroom;
  });
}

document.addEventListener('DOMContentLoaded', restore_options);
document.getElementById('saveOptions').addEventListener('click',
    save_options);
